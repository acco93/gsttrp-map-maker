package controller;

import java.util.List;

public interface EventSource {

	boolean discardUnsavedWork();

	void printInfo(List<String> info);

	boolean askForOverwriteFile();

}
