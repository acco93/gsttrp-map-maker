package controller;

import cli.Cli;
import cli.CliInterface;
import io.IO;
import logger.Logger;
import model.Model;
import model.ModelInterface;
import model.Property;
import model.VertexType;
import view.ViewInterface;

public class Controller implements ControllerInterface {
	
	private ModelInterface model;
	private ViewInterface view;
	private CliInterface console;
	
	public Controller() {
		model = new Model();
		//view = new View(this);
		console = new Cli(this);
		console.loop();
	}

	@Override
	public synchronized void shutdown(EventSource source) {
		
		if(model.hasUnsavedWork()) {
			if(!source.discardUnsavedWork()) {
				return;
			}	
		}

		Logger.get().info("Performing clean up and shutting down.");
		System.exit(0);
		
	}

	@Override
	public synchronized void load(String filePath, EventSource source) {
		
		if(model.hasUnsavedWork()) {
			if(!source.discardUnsavedWork()) {
				return;
			}	
		}

		model.reset();
		
		Logger.get().info("Loading " + filePath + " ... ");
		boolean ok = IO.loadInstance(filePath, this);
		
		if(ok) {
			Logger.get().info("Instance successfully loaded.");
		} else {
			Logger.get().info("An error occurred while parsing the instance. Aborted.");
		}
		
	}

	@Override
	public synchronized void info(EventSource source) {
		
		source.printInfo(model.getInfo());
		
	}

	@Override
	public synchronized void add(VertexType vertexType, int x, int y, int q) {
	
		model.add(vertexType, x, y, q);
		
	}

	@Override
	public void remove(VertexType vertexType, int x, int y, int q) {
		model.remove(vertexType, x, y, q);
		
	}

	@Override
	public void store(String filePath, EventSource source) {
		
		Logger.get().info("Storing to " + filePath + " ... ");
		
		if(IO.fileExists(filePath)) {
			if(!source.askForOverwriteFile()) {
				return;
			}	
		}
		
	}

	@Override
	public void set(Property property, int value) {
		
		model.set(property, value);
		
		
	}

	@Override
	public void reset(EventSource source) {
		
		if(model.hasUnsavedWork()) {
			if(!source.discardUnsavedWork()) {
				return;
			}	
		}

		model.reset();

		Logger.get().info("Cleared.");
		
	}
	
}
