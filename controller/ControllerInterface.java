package controller;

import cli.Cli;
import model.Property;
import model.VertexType;

public interface ControllerInterface {

	void load(String filePath, EventSource source);

	void info(EventSource source);

	void shutdown(EventSource source);

	void add(VertexType vertexType, int x, int y, int q);

	void remove(VertexType vertexType, int x, int y, int q);

	void store(String filePath, EventSource source);

	void set(Property property, int value);

	void reset(EventSource source);

}
