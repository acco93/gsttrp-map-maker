package model;

import java.util.List;

public interface ModelInterface {

	boolean hasUnsavedWork();

	List<String> getInfo();

	void add(VertexType vertexType, int x, int y, int q);

	void remove(VertexType vertexType, int x, int y, int q);

	void reset();

	void set(Property property, int value);

}
