package model;

public enum VertexType {
	depot("depot"),
	truckCustomer("truck-customer"),
	vehicleCustomerNoPark("vehicle-customer-no-park"),
	vehicleCustomerYesPark("vehicle-customer-yes-park"),
	satellite("satellite");
	
	private String text;
	
	private VertexType(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

}
