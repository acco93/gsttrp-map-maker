package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import logger.Logger;

public class Model implements ModelInterface {

	private boolean isSavedWork;

	private Optional<Integer> truckCapacity;
	private Optional<Integer> trailerCapacity;
	
	private Optional<Vertex> depot;
	private List<Vertex> satellites;
	private List<Vertex> truckCustomers;
	private List<Vertex> vehicleCustomersNoPark;
	private List<Vertex> vehicleCustomersYesPark;

	public Model() {

		this.isSavedWork = false;
		this.depot = Optional.empty();
		this.truckCapacity = Optional.empty();
		this.trailerCapacity = Optional.empty();
		
		this.satellites = new ArrayList<>();
		this.truckCustomers = new ArrayList<>();
		this.vehicleCustomersNoPark = new ArrayList<>();
		this.vehicleCustomersYesPark = new ArrayList<>();

	}

	@Override
	public boolean hasUnsavedWork() {
		return !(isEmpty() || isSavedWork);
	}

	@Override
	public List<String> getInfo() {

		List<String> list = new ArrayList<>();

		if (this.isEmpty()) {

			list.add("Blank instance.");

		} else {

			list.add("Truck capacity = " + (truckCapacity.isPresent() ? truckCapacity.get().toString() : "undefined"));
			list.add("Trailer capacity = " + (trailerCapacity.isPresent() ? trailerCapacity.get().toString() : "undefined"));
			list.add("Depot = " + (depot.isPresent() ? "1":"0"));
			list.add("Satellites = " + this.satellites.size());
			list.add("Truck customers = "+this.truckCustomers.size());
			list.add("Vehicle customers without parking facility = " + this.vehicleCustomersNoPark.size());
			list.add("Vehicle customers with parking facility = " + this.vehicleCustomersYesPark.size());
			list.add("------");
			list.add("Changes saved to filesystem = " + isSavedWork);

		}

		return list;
	}

	private boolean isEmpty() {

		if (!depot.isPresent() && satellites.isEmpty() && truckCustomers.isEmpty() && vehicleCustomersNoPark.isEmpty()
				&& vehicleCustomersYesPark.isEmpty()) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public void add(VertexType vertexType, int x, int y, int q) {
		
		Vertex vertex = new Vertex(vertexType, x, y, q);

		switch (vertexType) {
		case depot:
			depot = Optional.of(vertex);
			break;
		case satellite:
			satellites.add(vertex);
			break;
		case truckCustomer:
			truckCustomers.add(vertex);
			break;
		case vehicleCustomerNoPark:
			vehicleCustomersNoPark.add(vertex);
			break;
		case vehicleCustomerYesPark:
			vehicleCustomersYesPark.add(vertex);
			break;
		default:
			break;

		}
		
		
		Logger.get().info("Vertex " + vertex + " successfully added.");
		
	}

	@Override
	public void remove(VertexType vertexType, int x, int y, int q) {
		
		Vertex vertex = new Vertex(vertexType, x, y, q);
		
		boolean success = false;
		List<Vertex> list = null;
		
		switch (vertexType) {
		case depot:
			if(depot.isPresent() && vertex.equals(depot.get())) {
				depot = Optional.empty();
				success = true;
			}
			break;
		case satellite:
			list = this.satellites;
			break;
		case truckCustomer:
			list = this.truckCustomers;
			break;
		case vehicleCustomerNoPark:
			list = this.vehicleCustomersNoPark;
			break;
		case vehicleCustomerYesPark:
			list = this.vehicleCustomersYesPark;
			break;
		default:
			break;
		}
		
		if(list != null) {
			success = list.remove(vertex);
		}
		
		if(success) {
			Logger.get().info("Vertex " + vertex + " successfully deleted.");
		} else {
			Logger.get().info("Vertex " + vertex + " not found.");
		}
		
	}

	@Override
	public void reset() {
		
		this.isSavedWork = false;
		this.depot = Optional.empty();
		this.truckCapacity = Optional.empty();
		this.trailerCapacity = Optional.empty();
		
		this.satellites.clear();
		this.truckCustomers.clear();
		this.vehicleCustomersNoPark.clear();
		this.vehicleCustomersYesPark.clear();

		
	}

	@Override
	public void set(Property property, int value) {
		
		switch (property) {
		case trailerCapacity:
			this.trailerCapacity = Optional.of(value);
			break;
		case truckCapacity:
			this.truckCapacity = Optional.of(value);
			break;
		default:
			break;

		}
		
	}

}
