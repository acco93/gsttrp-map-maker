package model;

public class Vertex {
	private int x;
	private int y;
	private VertexType type;
	private int load;

	public Vertex(VertexType type, int x, int y, int load) {
		this.x = x;
		this.y = y;
		this.type = type;
		this.load = load;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public VertexType getType() {
		return type;
	}

	public int getLoad() {
		return load;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + load;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (load != other.load)
			return false;
		if (type != other.type)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ", " + load + ", " + type + ")";
	}
}
