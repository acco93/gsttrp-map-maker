package logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Logger {

	static Logger instance = new Logger();
	
	private Logger() {
		
	}
	
	public static Logger get() {
		return instance;
	}
	
	public void info(String text) {
		System.out.println("[ " + getCurrentTime()+ " ] " + text);
	}

	private String getCurrentTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	
}
