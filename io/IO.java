package io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import controller.ControllerInterface;
import model.Instance;
import model.Property;
import model.Vertex;
import model.VertexType;

public class IO {

	public static boolean fileExists(String filePath) {
		return new File(filePath).exists();
	}
	
	public static boolean loadInstance(String filePath, ControllerInterface controller) {
		File file = new File(filePath);

		try {
			Scanner sc = new Scanner(file);


		    String line = sc.nextLine().trim();

		    // do whatever processing at the end of each line
		    String[] tokens = line.split("\\s");
		    
		    int truckCustomers = Integer.parseInt(tokens[0]);
			int satellites = Integer.parseInt(tokens[1]) - 1;
			int vehicleCustomersNoPark = 0;
			int vehicleCustomersYesPark= 0;
				
				
			try {
				
				vehicleCustomersNoPark =Integer.parseInt(tokens[2]);
				vehicleCustomersYesPark = Integer.parseInt(tokens[3]);
				
			}catch (Exception e) {
				vehicleCustomersNoPark = 0;
				vehicleCustomersYesPark= 0;

			}


			controller.set(Property.truckCapacity, sc.nextInt());
			controller.set(Property.trailerCapacity, sc.nextInt());


			controller.add(VertexType.depot, sc.nextInt(), sc.nextInt(), 0);
			
			for (int i = 0; i < satellites; i++) {
			
				controller.add(VertexType.satellite, sc.nextInt(), sc.nextInt(), 0);
				
			}

			for (int i = 0; i < truckCustomers; i++) {
				controller.add(VertexType.truckCustomer, sc.nextInt(), sc.nextInt(), sc.nextInt());
			}

			for (int i = 0; i < vehicleCustomersNoPark; i++) {
				controller.add(VertexType.vehicleCustomerNoPark, sc.nextInt(), sc.nextInt(), sc.nextInt());
			}
			
			for (int i = 0; i < vehicleCustomersYesPark; i++) {
				controller.add(VertexType.vehicleCustomerYesPark, sc.nextInt(), sc.nextInt(), sc.nextInt());
			}
		
			sc.close();

		} catch (Exception e) {
			return false;
		}


		return true;
	}

		
}
