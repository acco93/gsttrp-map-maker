/**
 * 
 */
package main;

import controller.Controller;
import logger.Logger;

/**
 * @author acco
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Logger.get().info("Starting the application.");

		new Controller();
		
	}

}
