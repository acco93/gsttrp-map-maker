package view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import controller.ControllerInterface;

public class View extends JFrame implements ViewInterface {

	private ControllerInterface controller;

	public View(ControllerInterface controller) {
		this.controller = controller;
		this.setTitle("Map Maker");
		this.setSize(new Dimension(600, 400));
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		//this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		
		this.setVisible(true);
	}

}
