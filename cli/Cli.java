package cli;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;

import controller.ControllerInterface;
import logger.Logger;
import model.Vertex;
import model.VertexType;

public class Cli implements CliInterface {

	private ControllerInterface controller;
	private Scanner scanner;

	public Cli(ControllerInterface controller) {
		this.controller = controller;

		Logger.get().info("Command-line interpreter is on. Type man for the list of commands.");

	}

	@Override
	public void loop() {

		scanner = new Scanner(System.in);

		while (true) {

			eval(parse(read(scanner)));

		}

	}

	
	
	@Override
	protected void finalize() throws Throwable {
		scanner.close();
	}

	private String read(Scanner scanner) {
		System.out.print("$ ");
		String line = scanner.nextLine();
		return line;
	}

	private Command parse(String read) {

		String[] tokens = read.split(" ");

		if (tokens[0].equals(CommandType.exit.name()) && tokens.length == 1) {
			return new Command(CommandType.exit);
		}

		if(tokens[0].equals(CommandType.info.name()) && tokens.length == 1) {
			return new Command(CommandType.info);
		}
		
		if(tokens[0].equals(CommandType.man.name()) && tokens.length == 1) {
			return new Command(CommandType.man);
		}
		
		if(tokens[0].equals(CommandType.add.name()) && tokens.length == 4) {
			try {
			return new Command(CommandType.add, tokens[1], Integer.parseInt(tokens[2]), Integer.parseInt(tokens[3]), Integer.parseInt(tokens[4]));
			} catch(Exception e) {
				System.out.println("An error occurred while parsing the command. Did you follow the command signature?");
			}
		}

		if(tokens[0].equals(CommandType.remove.name()) && tokens.length == 4) {
			try {
			return new Command(CommandType.remove, tokens[1], Integer.parseInt(tokens[2]), Integer.parseInt(tokens[3]), Integer.parseInt(tokens[4]));
			} catch(Exception e) {
				System.out.println("An error occurred while parsing the command. Did you follow the command signature?");
			}
		}
		
		if (tokens[0].equals(CommandType.store.name()) && tokens.length == 2) {
			return new Command(CommandType.store, tokens[1]);
		}
		
		if (tokens[0].equals(CommandType.reset.name()) && tokens.length == 1) {
			return new Command(CommandType.reset);
		}
		
		
		if (tokens[0].equals(CommandType.load.name()) && tokens.length == 2) {
			return new Command(CommandType.load, tokens[1]);
		}

		return new Command(CommandType.unknown);
	}

	private void eval(Command command) {

		switch (command.getType()) {
		case exit:
			controller.shutdown(this);
			break;
		case load:
			controller.load(command.getStringParam1(), this);
			break;
			
		case store:
			controller.store(command.getStringParam1(), this);
			break;
			
		case add:
			
			vertexTypeIsValid(command.getStringParam1()).ifPresent((vertexType)-> {
				controller.add(vertexType, command.getIntParam1(), command.getIntParam2(),command.getIntParam3());			
			});		

			break;
		case remove:
			
			vertexTypeIsValid(command.getStringParam1()).ifPresent((vertexType)-> {
				controller.remove(vertexType, command.getIntParam1(), command.getIntParam2(),command.getIntParam3());			
			});		

			break;			
		case man:
			for(CommandType commandType : CommandType.values()) {
				System.out.println(commandType.getDescription());
			}
			break;
		case reset:
			controller.reset(this);
			break;
		case info:
			controller.info(this);
			break;
		default:
			System.out.println("Unknown command.");
			break;
		}

	}

	private Optional<VertexType> vertexTypeIsValid(String string) {

		for(VertexType type : VertexType.values()) {
			if(type.getText().compareTo(string) == 0) {
				return Optional.of(type);
			}
		}
		

		System.out.println("Unknown vertex type. Available are");
		for(VertexType type : VertexType.values()) {
			System.out.println(type.getText());
		}

		
		return Optional.empty();
		
	}

	@Override
	public boolean discardUnsavedWork() {
		
		System.out.println("[!] There is unsaved work. Would you like to discard it?");
		System.out.println();
		System.out.println("\tyes");
		System.out.println("\tno (default)");
		System.out.println();
		System.out.print("Type your answer: ");

		String line = scanner.nextLine();

		boolean discard = false;
		
		if (line.compareTo("yes") == 0) {
			Logger.get().info("Unsaved changes have been discarded.");
			discard = true;
		} else {
			Logger.get().info("Unsaved changes are still here.");
		}

		return discard;
	}

	@Override
	public void printInfo(List<String> info) {

		for (String item : info) {
			System.out.println(item);
		}

	}

	@Override
	public boolean askForOverwriteFile() {
		System.out.println("[!] The selected path contains an existing file. Would you like to overwrite it?");
		System.out.println();
		System.out.println("\tyes");
		System.out.println("\tno (default)");
		System.out.println();
		System.out.print("Type your answer: ");

		String line = scanner.nextLine();

		boolean discard = false;
		
		if (line.compareTo("yes") == 0) {
			Logger.get().info("Overwritten.");
			discard = true;
		} else {
			Logger.get().info("The old file is still here. Safe.");
		}

		return discard;
	}

}
