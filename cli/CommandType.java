package cli;

public enum CommandType {
	/**
	 * Closes the application.
	 */
	exit("exit: closes the application."),
	/**
	 * Load a file from file system.
	 */
	load("load <file path>: loads a file from the filesystem."),
	/**
	 * Display info related to the current instance.
	 */
	info("info: displays current instance properties."),
	/**
	 * Display the list of commands.
	 */
	man("man: shows the list of commands."),
	/**
	 * Add a new vertex to the instance.
	 */
	add("add <vertex type> <x> <y> (load)"),
	remove("remove <vertex type> <x> <y> (load)"),
	store("store <file path>"),
	reset("reset: create a new blank instance"),
	/**
	 * Fallback, unknown command.
	 */
	unknown("unknown: fallback command.");
	
	
	
	private String description;

	private CommandType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
	
}
