package cli;

import controller.EventSource;

public interface CliInterface extends EventSource {

	void loop();
	
}
