package cli;

public class Command {

	private static final String undefined = "undefined";
	private static final int undefinedInteger = -1;
	
	private CommandType type;
	private String stringParam1;
	private int intParam1;
	private int intParam2;
	private int intParam3;

	public Command(CommandType type, String stringParam1, int intParam1, int intParam2, int intParam3) {
		this.type = type;
		this.stringParam1 = stringParam1;
		this.intParam1 = intParam1;
		this.intParam2 = intParam2;
		this.intParam3 = intParam3;
	}
	
	public Command(CommandType type, String param1) {
		this(type, param1, undefinedInteger,undefinedInteger, undefinedInteger);
	}
	
	public Command(CommandType type) {
		this(type, Command.undefined);
	}

	public CommandType getType() {
		return type;
	}


	public String getStringParam1() {
		return stringParam1;
	}

	public int getIntParam1() {
		return intParam1;
	}

	public int getIntParam2() {
		return intParam2;
	}

	public int getIntParam3() {
		return intParam3;
	}

	
	
	
}
